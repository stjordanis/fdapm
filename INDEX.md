# FDAPM

APM / ACPI control/info, energy saving TSR/control, cache flush, rebooting... {a replacement for MS-DOS POWER}


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## FDAPM.LSM

<table>
<tr><td>title</td><td>FDAPM</td></tr>
<tr><td>version</td><td>2009sep11 (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2009-09-11</td></tr>
<tr><td>description</td><td>APM / ACPI control/info, energy saving TSR/control, cache flush, rebooting... {a replacement for MS-DOS POWER}</td></tr>
<tr><td>keywords</td><td>APM, ACPI, power, battery, energy saving, spin down, dpms screen, throttle, reboot, shutdown</td></tr>
<tr><td>author</td><td>Eric Auer mceric_at_users.sourceforge.net</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer mceric_at_users.sourceforge.net</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.auersoft.eu/soft/</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/fdapm/</td></tr>
<tr><td>platforms</td><td>DOS (nasm)</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
